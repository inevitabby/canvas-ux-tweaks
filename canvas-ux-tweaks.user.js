// ==UserScript==
// @name	Canvas UX Tweaks
// @namespace	https://gitlab.com/inevitabby/canvas-ux-tweaks
// @version	1.0
// @description	Various client-side tweaks to Canvas LMS for my personal use.
// @author	Inevitabby
// @match	https://*.instructure.com/*
// @match	https://canvas.*.edu/*
// @icon	https://duckduckgo.com/ip3/www.canvas.instructure.com.ico
// @run-at	document-start
// @grant	none
// ==/UserScript==

(function () {
	"use strict";
	// ===
	// CSS
	// ===
	let css = `
	/* === Visibility / Responsiveness === */
	/* Responsiveness / Visibility (for use with Darkreader Add-on!) */
	.planner-completed-items { /* UX: Make completed items in the dashboard in list view greyed out unless hovered over */
		opacity: 0.21 !important;
		transition: opacity 169ms !important;
	}
	.planner-completed-items:hover {
		opacity: 0.99 !important;
	}
	/* UX: Make notification bubbles green & bouncy */
	div.NotificationBadge-styles__activityIndicator.NotificationBadge-styles__hasBadge div span { 
		zoom: 125% !important;
		background-color: lightgreen !important;
		-webkit-animation: pulsate-fwd 690ms ease-in-out infinite both !important;
		animation: pulsate-fwd 690ms ease-in-out infinite both !important;
	}
	/* UX: Replace 600ms bounce animation with a faster sliding one */
	body.primary-nav-transitions .menu-item__text { 
		transition: transform 69ms cubic-bezier(0.21, 0.420, 0.69, 1.275),opacity 42ms !important;
		transition-delay: 21ms !important;
	}
	/* Visibility: Disable obnoxious sidebar coloring */
	.ic-app-header { 
		background-color: unset !important
	}
	/* Visibility: Make SVGs in left navbar visible in Dark Reader */
	header .ic-icon-svg { 
		fill: white !important
	}

	/* === Focused UI === */
	/* UX: Remove a bunch of unwanted elements */
	#immersive_reader_mount_point,	/* UX: Remove the immersive reader button */
	#new_activity_button, /* UX: Remove the new activity button. Button doesn't even work because most profs don't remove/delete old assignments so it just links to clutter */
	#footer, /* UX: Remove the footer, which is pointless linkspam for Instructure LTI's social media & legal stuff */
	#global_nav_history_link, /* UX: Remove the "History" button from the leftmost sidebar */
	#global_nav_help_link, /* UX: Remove "Help" button from leftmost sidebar */
	#primaryNavToggle, /* UX: Removes button to toggle expanding the leftmost sidebar */
	#conversation-actions button[disabled], /* UX: Remove buttons that were already disabled by your organization in the inbox */
	#course_show_secondary a.btn.button-sidebar-wide , /* UX: Remove "View Course Stream", "View Course Calendar", "View Course Notifications" buttons from course view */
	a.ic-app-header__logomark { /* UX: Remove the clipart-looking icon that is your organization's logo from the leftmost sidebar */
		display: none !important;
	}
	/* UX: Remove class banner images from dashboard in list view & card view (makes them solid colors) */
	div.Grouping-styles__root.planner-grouping a, div.ic-DashboardCard__header_image { 
		background-image: none !important;
		height: auto !important;
	}

	/* === Animations === */
	@-webkit-keyframes pulsate-fwd {
		0% {
		-webkit-transform: scale(1);
		transform: scale(1);
		}
		50% {
		-webkit-transform: scale(1.1);
		transform: scale(1.1);
		}
		100% {
		-webkit-transform: scale(1);
		transform: scale(1);
		}
	}
	@keyframes pulsate-fwd {
		0% {
		-webkit-transform: scale(1);
		transform: scale(1);
		}
		50% {
		-webkit-transform: scale(1.1);
		transform: scale(1.1);
		}
		100% {
		-webkit-transform: scale(1);
		transform: scale(1);
		}
	}
`;
	// ===============================
	// Kill Elements & Their Listeners
	// ===============================
	const killList = [
		"mobile-header",
		"immersive_reader_mount_point"
	];
	const observer = new MutationObserver(function(mutations_list) {
		mutations_list.forEach(function(mutation) {
			mutation.addedNodes.forEach(function(elem) {
				if (elem.id && killList.indexOf(elem.id) !== -1) {
					elem.cloneNode(true); // Kill event listeners
					elem.remove(); // Remove element
				}
			});
		});
	});
	observer.observe(document, { subtree: true, childList: true });
	// =================
	// JavaScript Tweaks
	// =================
	window.addEventListener("DOMContentLoaded", function () {
		// Bug: Scrolling to the top of the announcements causes the entire page to jump downwards (making it hard to close announcements)
		// Fix: All wheel event listeners are now disabled when on the dashboard.
		if (document.querySelector("#announcementWrapper") !== null) {
			window.addEventListener(
				"wheel",
				function (event) {
					event.stopImmediatePropagation();
				},
				true
			);
		}
		// UX: Always collapse leftmost sidebar
		document.querySelector("body").classList.remove("primary-nav-expanded");
		// UX: When visiting the dashboard, automatically scroll past the announcements down to the actual dashboard
		let y = document.querySelector("#dashboard_header_container");
		if (y !== null) {
			window.scroll({
				top: y.getBoundingClientRect().top + window.scrollY,
				behavior: "smooth",
			});
		}
		// EXPERIMENTAL: Prevent Event Propagation
		const preventList = [
			"click",
			"blur",
			"focus",
			"mousedown",
			"mousemove",
			"mouseout",
			"mouseover",
			"mouseup",
			"visibilitychange"
		];
		preventList.forEach((event) => {
			document.documentElement.removeEventListeners(event);
			window.removeEventListeners(event);
		});
	});
	// ==========================
	// Event Listener Interceptor
	// ==========================
	let listeners = [];
	EventTarget.prototype.addEventListenerBase = EventTarget.prototype.addEventListener;
	EventTarget.prototype.addEventListener = function(type, listener) {
		listeners.push({target: this, type: type, listener: listener});
		this.addEventListenerBase(type, listener);
	};
	EventTarget.prototype.removeEventListeners = function(targetType) {
		for (var index = 0; index != listeners.length; index++) {
			var item = listeners[index];
			var target = item.target;
			var type = item.type;
			var listener = item.listener;
			if (target == this && type == targetType) {
				this.removeEventListener(type, listener);
			}
		}
	}
	// ==========
	// Inject CSS
	// ==========
	document.addEventListener("DOMContentLoaded", injectStyle, false);
	document.addEventListener("load", injectStyle, false);
	document.addEventListener("focus", injectStyle, false);
	function injectStyle() {
		if (document.querySelector("#canvasLmsTweaks") !== null) { // Don't proceed if already injected
			return;
		}
		const styleElem = document.createElement("style");
		styleElem.setAttribute("type", "text/css");
		styleElem.setAttribute("id", "canvasLmsTweaks");
		styleElem.appendChild(document.createTextNode(css));
		document.querySelector("head").appendChild(styleElem);
	}
})();

