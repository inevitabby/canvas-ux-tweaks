# Canvas UX Tweaks

Various client-side tweaks to Canvas LMS for my personal use.

## History

Most of this script was written a few years ago when I was in my Sophomore year of high school during Distanced Learning.

This userscript exists to:

1. Make Canvas match the rest of my desktop's theming (by using [Darkreader](https://github.com/darkreader/darkreader) to style webpages to be [Catppuccin Mocha](https://github.com/catppuccin/dark-reader)), and
2. To remove elements I don't use for readability.

